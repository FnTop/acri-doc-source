import { SearchPlugin } from "vitepress-plugin-search";
import { defineConfig } from "vitepress";
import flexSearchIndexOptions from "flexsearch";
export default {
  //搜索插件 https://www.npmjs.com/package/vitepress-plugin-search
  //选项设置 https://github.com/nextapps-de/flexsearch#options
  vite: {
    plugins: [
      SearchPlugin({
        ...flexSearchIndexOptions,
        previewLength: 120, //搜索结果预览长度
        buttonLabel: "搜索",
        placeholder: "输入关键词",
        tokenize: 'full',
      })]
  },
  title: 'Acri-doc',
  base: '/acri-doc/',
  head: [
    ['link', { rel: 'icon', href: '/acri-doc/logo.ico' }]
  ],
  lastUpdated: true,
  themeConfig: {
    // search: {
    //   provider: 'local',
    //   option:{
    //     previewLength: 120
    //   }
    // },
    // search: {
    // see https://vitepress.dev/reference/default-theme-search
    // provider: 'local'
    // provider: 'algolia',
    // options: {
    //   appId: 'P0xxxxxxQ5',
    //   apiKey: 'cc79aexxxxxxxxxxe21a489d034',
    //   indexName: 'Acrixx'
    // }
    // },
    returnToTopLabel: '返回顶部',
    outline: {
      level: "deep",
      label: "目录", // 右侧大纲标题文本配置
    },
    socialLinks: [{ icon: 'github', link: "https://github.com/FnTop/Acri" }],
    // editLink: {
    //   pattern: 'https://github.com/vuejs/vitepress/edit/main/docs/:path',
    //   text: 'Edit this page on GitHub'
    // },
    lastUpdatedText: '上次更新',
    footer: {
      message: 'Released 1.3.0 ',
      copyright: 'Copyright © 2023 Acri'
    },
    nav: [
      { text: '首页', link: '/' },
      { text: '文档', link: '/guide/' },
      { text: 'AOP', link: '/guide/aop' },
      // { text: 'crawler', link: '/guide/crawler' },
      {
        text: 'Release 1.4.0', items: [
          { text: '1.4.0', link: 'https://central.sonatype.com/artifact/cn.fntop/acri-spring-boot-starter/1.4.0' },
          { text: '1.3.0', link: 'https://central.sonatype.com/artifact/cn.fntop/acri-spring-boot-starter/1.3.0' },
          { text: '1.2.0', link: 'https://central.sonatype.com/artifact/cn.fntop/acri-core/1.2.0' },
          { text: '1.1.0', link: 'https://central.sonatype.com/artifact/cn.fntop/acri-core/1.1.0' },
          { text: '1.0.0', link: 'https://central.sonatype.com/artifact/cn.fntop/acri-core/1.0.0' },
        ]
      },
      { text: '联系方式', link: '/guide/contact', activeMatch: '/guide/contact' }
      // ,{
      //   text: '下拉选择框',
      //   items: [
      //     { text: '', link: '/', activeMatch: '/guide' },
      //     { text: '选项2', link: 'http://www.baidu.com' }
      //   ]
      // }
    ],
    sidebar: {
      '/guide/': [
        {
          text: '指南',
          items: [
            { text: '文档', link: '/guide/' },
            { text: 'AOP切面', link: '/guide/aop' },
            { text: '版本日志', link: '/guide/version' },
            { text: '联系方式', link: '/guide/contact' }
          ]
        }
      ]
    },
    docFooter: { prev: '上一篇', next: '下一篇' }
  },
  markdown: {
    lineNumbers: true // 显示行号
  }
};
