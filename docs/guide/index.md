---
layout: doc
---

<div style="display: flex; justify-content: center;">  
<img style="margin: 5px 3px" width=200 height=100 src="../public/logo1.png" >
</div>
<p align='center'>🍍🍍注解式自定义请求/方法拦截🍍🍍</p>


## 🍌🍌介绍
Acri（全称Annotation Custom Request Interception）

一个注解即可实现请求的拦截处理或AOP增强

## 🍊🍊特点
* 一个`@Acri`或者`@Acries`注解搞定请求拦截
* 支持自定义拦截器扩展
* 一个`@AcriAspect`注解搞定AOP切面
* 支持自定义切面类、切面方法扩展
* 支持单个方法单拦截
* 支持单个方法多个拦截
* 支持多方法多拦截
* 支持类标注`@Acri` `@Acries`注解
* `@Acri` `@Acries`注解支持拦截覆盖和拦截合并

## 🫐🫐依赖
```xml
<dependency>
    <groupId>cn.fntop</groupId>
    <artifactId>acri-spring-boot-starter</artifactId>
    <version>1.4.0</version>
</dependency>

implementation 'cn.fntop:acri-spring-boot-starter:1.4.0'
//方式2
implementation group: 'cn.fntop', name: 'acri-spring-boot-starter', version: '1.4.0'
``` 




## 🍈🍈使用方式


### 添加注解

```java
@Acri(value = AcriStopWatchProcessor.class, before = true, during = true, after = true)
@GetMapping("/login")
public String login() {
    log.info("登录中");
    return "登录成功";
}
```
### 效果


<img style="margin: 5px 3px"  src="../public/img_1.png" >

## 🍐🍐注解说明
`@Acri(value = AcriStopWatchProcessor.class, before = true, during = true, after = true)`

| 配置项    | 默认值   | 备注                                 |
|:-------|:------|:-----------------------------------|
| value  | null  | 具体拦截处理器，实现AcriProcessor并注入Spring容器 |
| before | false | 是否处理请求前执行doBefore方法                |
| during | false | 是否处理请求方法后执行doDuring方法              |
| after  | false | 是否在返回响应后执行doAfter方法                |

`@Acries(acries = {@Acri(value = AcriStopWatchProcessor.class, before = true, after = true)
, @Acri(value = CustomProcessor.class, before = true, after = true)})` 
如果存在`@Acries`注解，则`@Acri`注解不起作用

| 配置项    | 默认值   | 备注                                 |
|:-------|:------|:-----------------------------------|
| acries  | null  | 多@Acri拦截 |

## 🍉🍉自定义拦截
```java
@Slf4j
@Component
public class CustomProcessor implements AcriProcessor {
    @Override
    public void doBefore(HttpServletRequest request, HttpServletResponse response, Object handler) {
        log.info("before");
    }

    @Override
    public void doDuring(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        log.info("doDuring");
    }
    @Override
    public void doAfter(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        log.info("doAfter");
    }
}
```

## 🍏🍏多拦截支持

```java
@Acries(acries = {@Acri(value = AcriStopWatchProcessor.class, before = true, after = true)
            , @Acri(value = CustomProcessor.class, before = true, after = true)})
@GetMapping("/login")
public String login() {
    log.info("登录中");
    return "登录成功";
}
```

## 🥥🥥拦截合并
在拦截器处理类不相同的情况下。及处理开启状态下类和方法上所有拦截处理类的`doBefore/doDuring/doAfter`进行合并处理

### 类合并

访问`test()`方法时，同时处理`DemoFallBack`和`DemoController`中的 `doBefore/doDuring/doAfter`，注解在类上。

```java
@RestController
@Slf4j
@Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = true))
@Acri(value = DemoController.class, before = true, after = true, during = true)
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    public String test() {
        log.info("test");
        return "success";
    }
}
/**
 * 或者只用一个@Acries也可以
 */
@Acries(acries = {@Acri(value = DemoFallBack.class, before = true, after = true,during = true),@Acri(value = DemoController.class, before = true, after = true,during = true)})
```


### 方法合并
注解在方法上

```java
@RestController
@Slf4j
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    @Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = true))
    @Acri(value = DemoController.class, before = true, after = true, during = true)
    public String test() {
        log.info("test");
        return "success";
    }
}
/**
 * 或者只用一个@Acries也可以
 */
@Acries(acries = {@Acri(value = DemoFallBack.class, before = true, after = true,during = true),@Acri(value = DemoController.class, before = true, after = true,during = true)})
```

### 类+方法合并

注解同时可以在方法和类上
```java
@RestController
@Slf4j
@Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = true))
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    @Acri(value = DemoController.class, before = true, after = true, during = true)
    public String test() {
        log.info("test");
        return "success";
    }

}
```


## 🍒🍒拦截覆盖

在拦截器处理类相同的情况下。覆盖原则为`方法覆盖类（优先@Acries覆盖@Acri）`，`@Acries覆盖@Acri`

### 方法覆盖类

以下会执行`DemoFallBack`的`before和after`,因为方法上的注解`during=false`为未开启，即使类上开启了`during=true`,访问test时也不会执行during，即`方法覆盖类`
```java
@RestController
@Slf4j
@Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = true))
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    @Acri(value = DemoFallBack.class, before = true, after = true, during = false)
    public String test() {
        log.info("test");
        return "success";
    }
}
/**
 * 或者将方法上的注解改为@Acries也是一样的
 */

@Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = true))
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    @Acries(acri=@Acri(value = DemoFallBack.class, before = true, after = true, during = false))
    public String test() {
        log.info("test");
        return "success";
    }
}
```
### @Acries覆盖@Acri

以下会执行`DemoFallBack`的`before和after`,因为方法上的`@Acries`注解`during=false`为未开启，即使方法上的`@Acri`注解开启了`during=true`,访问test时也不会执行during，即`@Acries覆盖@Acri`
```java
@RestController
@Slf4j
public class DemoController implements AcriProcessor {
    @RequestMapping("test")
    @Acries(acries = @Acri(value = DemoFallBack.class, before = true, after = true,during = false))
    @Acri(value = DemoFallBack.class, before = true, after = true, during = true)
    public String test() {
        log.info("test");
        return "success";
    }
}
```



